import json
import sys

from terraform_functions import *
from json_functions import flatten_json

d = {
    "HBA - Backoffice Api": ("C:/Users/Max/Documents/Repos/azuretools/src/Appsettings/middleware/test.json", "hba"),
    "HBO - Backoffice": ("C:/Users/Max/Documents/Repos/azuretools/src/Appsettings/backoffice/test.json", "hbo"),
    "HID - Identification Site": ("C:/Users/Max/Documents/Repos/azuretools/src/Appsettings/identificationSite/test.json", "hid"),
    "HIM - Intermediary Site": ("C:/Users/Max/Documents/Repos/azuretools/src/Appsettings/intermediarySite/test.json", "him"),
}


def print_rows(rows: list):
    for r in rows:
        print(r)


if __name__ == '__main__':
    for key, value in d.items():
        try:
            f = open(value[0], encoding='utf-8')
            json_data = json.loads(f.read())
        except (OSError, IOError) as e:
            print(e)
            sys.exit(-1)

        a = f"app_{value[1]}_"

        flattened = list(flatten_json(json_data))

        print("#####################\n")
        print(f"# {key}")
        print_rows(create_terraform_vars_file(flattened, a))
        print()

        print_rows(create_terraform_variable_list(flattened, a))
        print()

        print("app_settings = {")
        print_rows(create_terraform_app_settings_list(flattened, key_right=a))
        print("}")
        print("\n#####################\n")
