keyvault_level_sep = "-"
appsetting_level_sep = "__"

webapp_setting_sep = " \\\n"


def create_keyvault_add_cmd(rows: [tuple], keyvault_name: str):
    def create_row(key: [str], key_value: str):
        key_name = keyvault_level_sep.join(key)
        return f"az keyvault secret set --vault-name '{keyvault_name}' --name '{key_name}' --value '{key_value}'"

    return [create_row(r[0], r[1]) for r in rows]


def create_keyvault_delete_cmd(rows: [tuple], keyvault_name: str):
    def create_row(key: [str], key_value: str):
        key_name = keyvault_level_sep.join(key)
        return f"az keyvault secret delete --vault-name '{keyvault_name}' --name '{key_name}'"

    return [create_row(r[0], r[1]) for r in rows]


def create_keyvault_purge_cmd(rows: [tuple], keyvault_name: str):
    def create_row(key: [str], key_value: str):
        key_name = keyvault_level_sep.join(key)
        return f"az keyvault purge --vault-name '{keyvault_name}' --name '{key_name}'"

    return [create_row(r[0], r[1]) for r in rows]

def creat_appsetting_cmd(rows: [tuple], keyvault_name: str, app_service_name: str, resource_group: str):
    def create_row(key: [str]):
        settings_name = appsetting_level_sep.join(key)
        key_name = keyvault_level_sep.join(key)
        return f"'{settings_name}=@Microsoft.KeyVault(VaultName={keyvault_name};SecretName={key_name})'"

    settings = [create_row(r[0]) for r in rows]
    head = f"az webapp config appsettings set --name '{app_service_name}' --resource-group '{resource_group}' --settings "
    return head + webapp_setting_sep.join(settings)



def delete_appsetting_cmd(rows: [tuple], keyvault_name: str, app_service_name: str, resource_group: str):
    def create_row(key: [str]):
        settings_name = appsetting_level_sep.join(key)
        return f"'{settings_name}'"

    settings = [create_row(r[0]) for r in rows]
    head = f"az webapp config appsettings delete --name '{app_service_name}' --resource-group '{resource_group}' --settings "
    return head + webapp_setting_sep.join(settings)
