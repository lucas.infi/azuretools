import json
import sys

from azure_functions import create_keyvault_add_cmd, creat_appsetting_cmd, create_keyvault_delete_cmd, \
    create_keyvault_purge_cmd, delete_appsetting_cmd

from json_functions import flatten_json

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser("Generate commands to create a keyvault from a appsettings.json")
    parser.add_argument('--keyvault_name', type=str, required=True, help='Name of the keyvault')
    parser.add_argument('--resource_group', type=str, required=True,
                        help='ResourceGroup the keyvault and appservice are in')
    parser.add_argument('--app_service_name', type=str, required=True,
                        help='Name of the appservice whose settings are to be set')
    parser.add_argument('--path_to_appsettings', type=str, required=True, help='Path to the appsettings file to parse')

    parser.add_argument('--keyvault_purge', type=bool, required=False,
                        help='Generate keyvault delete commands')
    parser.add_argument('--keyvault_delete', type=bool, required=False, help='Generate keyvault pruge commands')

    parser.add_argument('--appsetting_remove', type=bool, required=False, help='Generate appsettings remove commands')

    args = parser.parse_args()

    keyvault_name = args.keyvault_name
    app_service_name = args.app_service_name
    resource_group = args.resource_group

    try:
        f = open(args.path_to_appsettings, encoding='utf-8')
        json_data = json.loads(f.read())
    except (OSError, IOError) as e:
        print(e)
        sys.exit(-1)

    flattened = list(flatten_json(json_data))

    rows = create_keyvault_add_cmd(flattened, keyvault_name)
    print("### Keyvault ###")
    for r in rows:
        print(r)
    print()

    if args.keyvault_delete:
        rows = create_keyvault_delete_cmd(flattened, keyvault_name)
        for r in rows:
            print(r)
        print()

    if args.keyvault_purge:
        rows = create_keyvault_purge_cmd(flattened, keyvault_name)
        for r in rows:
            print(r)
        print()

    print("### AppSettings ###")
    if args.appsetting_remove:
        cmd = delete_appsetting_cmd(flattened, keyvault_name, app_service_name, resource_group)
        print(cmd)
        print()

    cmd = creat_appsetting_cmd(flattened, keyvault_name, app_service_name, resource_group)
    print(cmd)
    print()
